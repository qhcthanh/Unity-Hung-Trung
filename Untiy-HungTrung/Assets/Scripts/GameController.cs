﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController {
    static GameController _instance = null;
   
    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameController();
            }
            return _instance;
        }
    }

    public int ActiveCharacter = 0;
    public SoundController soundController;

    public bool IsSoundOn
    {
        get { return PlayerPrefs.GetInt("config_sound_off") == 0; }
        set
        {
            if (value)
                PlayerPrefs.SetInt("config_sound_off", 0);
            else
                PlayerPrefs.SetInt("config_sound_off", 1);
        }
    }

    public bool IsBackgroundSoundOn
    {
        get { return PlayerPrefs.GetInt("config_backgroundsound_off") == 0; }
        set
        {
            if (value)
                PlayerPrefs.SetInt("config_backgroundsound_off", 0);
            else
                PlayerPrefs.SetInt("config_backgroundsound_off", 1);
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene("Game Success");
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level Scene");
    } 

    public void PlayGame()
    {
        SceneManager.LoadScene("Play Scene");
    }
}
