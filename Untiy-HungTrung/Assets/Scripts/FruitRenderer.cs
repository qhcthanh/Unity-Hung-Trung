﻿using UnityEngine;
using System.Collections;

public class FruitRenderer : MonoBehaviour {
    public GameObject[] fruits;
    public GameObject leftPosition;
    public GameObject rightPositon;
    public float testSpeedX;
    public float testSpeedY;
    public float remainTimeToRender;
    public float totalTimeToRender;

    // Use this for initialization
    void Start()
    {
        totalTimeToRender = 1.0f;
        remainTimeToRender = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        remainTimeToRender -= Time.deltaTime;
        if (remainTimeToRender <= 0)
        {
            RenderFruit();
            totalTimeToRender = Random.Range(0.2f, 1.2f);
            remainTimeToRender = totalTimeToRender;
        }
    }

    void RenderFruit()
    {
        GameObject fruit = fruits[Random.Range(0, fruits.Length)];
        int indexGun = Random.Range(0, 2);
        Transform pos = indexGun == 0 ? leftPosition.transform : rightPositon.transform;

        GameObject fruitClone = Instantiate(fruit, pos.position, Quaternion.identity) as GameObject;


        float tempSpeedX = Random.Range(2f, 5.5f);
        float tempSpeedY = Random.Range(3f, 8.5f);
        if (indexGun == 1)
            tempSpeedX = -tempSpeedX;

        fruitClone.GetComponent<Rigidbody2D>().velocity = new Vector2(tempSpeedX, tempSpeedY);
    }
}
