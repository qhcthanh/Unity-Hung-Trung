﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CanvasTextEffect : MonoBehaviour {
    public GameObject plusGoldEffectTextPrefab;
    public GameObject goldText;

    public void RenderPlusGoldTextEffect(Transform pos, int plusValue)
    {
        GameObject plusGoldEffect = Instantiate(plusGoldEffectTextPrefab, pos.position, Quaternion.identity) as GameObject;
        plusGoldEffect.GetComponent<Text>().text = String.Format("+{0}", plusValue);
        plusGoldEffect.transform.SetParent(this.transform);
        plusGoldEffect.transform.localScale = Vector3.one;

        var dir = goldText.transform.position - pos.position; //target.position - source.position
        plusGoldEffect.GetComponent<Rigidbody2D>().velocity = dir;
    }
}
