﻿using UnityEngine;
using System.Collections;

public class MainGameController : MonoBehaviour {

    public string PlayerName;
    public int PlayerColor;
    public int[] LevelFruitCount;
    public int[] LevelFruitNum;
    public float[] LevelSpeed;
    public int Level;
    public GameObject[] Fruit;
    public string[] FruitNeed;
    public int[] FruitNeedCouter;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    void Start () {
	       
	}
	
	void Update () {
	    
	}

    public void OnSelectLevel(int num)
    {
        Level = num;

    }
}
