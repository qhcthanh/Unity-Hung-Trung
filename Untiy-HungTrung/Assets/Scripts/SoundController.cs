﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    public AudioClip kachingSound;
    public AudioClip backgroundSound;
    public AudioClip increaseScore;
    AudioSource audioSource;

    static SoundController instance = null;

    void Awake()
    {
        if (instance == null)
        {
            GameController.Instance.soundController = instance = this;
            DontDestroyOnLoad(gameObject);
            audioSource = GetComponent<AudioSource>();
            if (!GameController.Instance.IsSoundOn)
                VolumeOff();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void playKaChingSound()
    {
        if (GameController.Instance.IsSoundOn)
            audioSource.PlayOneShot(kachingSound, 0.7f);
    }

    public void playIncreaseScoreSound()
    {
        if (GameController.Instance.IsSoundOn)
            audioSource.PlayOneShot(increaseScore);
    }

    public void VolumeOff()
    {
        audioSource.volume = 0;
        GameController.Instance.IsSoundOn = false;

    }

    public void VolumeOn()
    {
        audioSource.volume = 100;
        GameController.Instance.IsSoundOn = true;
    }

    public void playGameBackgroundSound()
    {
        audioSource.clip = backgroundSound;
        audioSource.loop = true;
        audioSource.Play();
    }
}
