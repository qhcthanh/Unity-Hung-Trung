﻿using UnityEngine;
using System.Collections;

public class DropDownCollider : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("fruit"))
        {
            Debug.Log("Fruit Drop " + col.gameObject.name);
            GameController.Instance.GameOver();
        }
        Destroy(col.gameObject);
    }
}
