﻿using UnityEngine;
using System.Collections;

public class GoldCollider : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("text_effect"))
        {
            Destroy(col.gameObject);
        }
    }
}
