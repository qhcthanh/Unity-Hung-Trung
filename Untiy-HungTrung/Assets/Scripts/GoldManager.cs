﻿using UnityEngine;

public class GoldController
{
    static string tagUserGold = "user_gold";

    public static int GetGold()
    {
        return PlayerPrefs.GetInt(tagUserGold);
    }

    public static void SetGold(int gold)
    {
        PlayerPrefs.SetInt(tagUserGold, gold);
    }

    public static void IncreaseGold(int amount)
    {
        int gold = GetGold();
        gold += amount;
        SetGold(gold);
    }

    public static void DecreaseGold(int amount)
    {
        int gold = GetGold();
        gold -= amount;
        if (gold <= 0)
            gold = 0;
        SetGold(gold);
    }
}
