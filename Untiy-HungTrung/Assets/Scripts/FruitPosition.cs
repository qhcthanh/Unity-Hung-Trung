﻿using UnityEngine;
using System.Collections;

public class FruitPosition : MonoBehaviour {
    
    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
