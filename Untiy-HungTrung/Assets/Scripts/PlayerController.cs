﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    
    public GameObject plusGoldEffectTextPrefab;
    public Sprite[] leftCharacters;
    public Sprite[] rightCharacters;
    public Sprite[] centerCharacters;

    public Canvas TextEffectCanvas;
    public GameObject PlayerName;

    float remainTimeToChangeCharacterStateToCenter = 0f;
    private float leftMost;
    private float rightMost;
    private float topMost;
    private float bottomMost;

    // Use this for initialization
    void Start()
    {
        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distance));
        Vector3 rightBottom = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        leftMost = leftTop.x;
        rightMost = rightBottom.x;
        topMost = leftTop.y;
        bottomMost = rightBottom.y;
    }

    // Update is called once per frame
    void Update()
    {
        MoveWithMouse();
    }

    void OnMouseDrag()
    {
        //MoveWithMouse();
    }

    private void MoveWithMouse()
    {
        float gameWidth = rightMost * 2;
        Vector3 paddlePos = new Vector3(0f, this.transform.position.y, transform.position.z);
        float mousePosInBlock = Input.mousePosition.x / Screen.width * gameWidth;
        paddlePos.x = mousePosInBlock - gameWidth / 2;

        //Update Character State: Center, Left, Right
        UpdateCharacterState(this.transform.position.x, paddlePos.x);

        //Change new position
        this.transform.position = paddlePos;
        PlayerName.GetComponent<RectTransform>().anchoredPosition = new Vector2( paddlePos.x * 76.25f , PlayerName.GetComponent<RectTransform>().anchoredPosition.y);
    }

    void UpdateCharacterState(float oldX, float newX)
    {
        if (Mathf.Abs(oldX - newX) < 0.01)
        {
            //Do nothing
        }
        else if (newX - oldX > 0)//move right
        {
            GetComponent<SpriteRenderer>().sprite = rightCharacters[GameController.Instance.ActiveCharacter];
            remainTimeToChangeCharacterStateToCenter = 0.5f;
        }
        else //move left
        {
            GetComponent<SpriteRenderer>().sprite = leftCharacters[GameController.Instance.ActiveCharacter];
            remainTimeToChangeCharacterStateToCenter = 0.5f;
        }

        remainTimeToChangeCharacterStateToCenter -= Time.deltaTime;
        if (remainTimeToChangeCharacterStateToCenter <= 0)
            GetComponent<SpriteRenderer>().sprite = centerCharacters[GameController.Instance.ActiveCharacter];
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject gameObj = col.gameObject;
        if (gameObj.CompareTag("fruit")) //catch the fruit
        {
            Fruit fruit = gameObj.GetComponent<Fruit>();
            int plusValue = fruit.goldPlus;
            RenderPlusGoldEffect(gameObj.transform, plusValue);
            GoldController.IncreaseGold(plusValue);
            if (GameController.Instance.soundController)
                GameController.Instance.soundController.playIncreaseScoreSound();

            Destroy(gameObj);
        }
        else if (gameObj.CompareTag("bomb")) //catch the bomb
        {
            Destroy(gameObj);
            Debug.Log("Game over: Receive a bomb");
            GameController.Instance.GameOver();
        }
    }

    private void RenderPlusGoldEffect(Transform pos, int plusValue)
    {
        TextEffectCanvas.GetComponent<CanvasTextEffect>().RenderPlusGoldTextEffect(pos, plusValue);

        //GameObject plusGoldEffect = Instantiate(plusGoldEffectTextPrefab, pos.position, Quaternion.identity) as GameObject;
        //plusGoldEffect.GetComponent<Text>().text = String.Format("+{0}", plusValue);
        //plusGoldEffect.transform.SetParent(TextEffectCanvas.transform);
    }
}
