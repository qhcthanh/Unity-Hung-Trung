﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoxTimerController : MonoBehaviour {
    public Text minText;
    public Text secText;

    int time = 90;
    float fTime = 90f;
    bool gameOver = false;
    
	void Start () {

	}
	
	void Update () {
        if (gameOver)
            return;

        fTime -= Time.deltaTime;
        if (fTime <= 0)
        {
            Debug.Log("Game Over: Time over");
            GameController.Instance.GameOver();
            gameOver = true;
        }

        time = (int)fTime;
        minText.text = (time / 60).ToString("D2");
        secText.text = (time % 60).ToString("D2");
	}
}
