﻿using UnityEngine;
using System.Collections;

public class LevelSceneBackground : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }
}
