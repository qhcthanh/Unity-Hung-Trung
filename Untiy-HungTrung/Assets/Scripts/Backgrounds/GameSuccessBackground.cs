﻿using UnityEngine;
using System.Collections;

public class GameSuccessBackground : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
    }
}
