﻿using UnityEngine;
using System.Collections;

public class PlaySceneController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playGameBackgroundSound();
    }
}
