﻿using UnityEngine;
using System.Collections;

public class GameSuccessSceneController : MonoBehaviour {

	public void OnRetry()
    {
        GameController.Instance.PlayGame();
    }

    public void OnContinue()
    {

    }

    public void OnShare()
    {

    }

    public void OnMore()
    {

    }

    public void OnClose()
    {

    }

    public void OnStar()
    {

    }
}
