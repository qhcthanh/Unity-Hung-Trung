﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSceneController : MonoBehaviour {

    public GameObject mainCharacter;
    public Sprite[] characters;

    public Image iconSoundHandler;
    public Image iconBackgroundSoundHandler;
    public Sprite iconSound;
    public Sprite iconSoundMute;
    public Sprite iconBackgroundSound;
    public Sprite iconBackgroundSoundMute;

    public void Start()
    {
        mainCharacter.GetComponent<Image>().sprite = characters[GameController.Instance.ActiveCharacter];
        UpdateIconSound();
        UpdateIconBackgroundSound();
    }

    private void UpdateIconSound()
    {
        iconSoundHandler.sprite = GameController.Instance.IsSoundOn ? iconSound : iconSoundMute;
    }

    private void UpdateIconBackgroundSound()
    {
        iconBackgroundSoundHandler.sprite = GameController.Instance.IsBackgroundSoundOn ? iconBackgroundSound : iconBackgroundSoundMute;
    }

    public void OnPlay()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
        GameController.Instance.PlayGame();
    }

    public void OnClose()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
    }

    public void OnBackgroundSound()
    {
        GameController.Instance.IsBackgroundSoundOn = !GameController.Instance.IsBackgroundSoundOn;
        UpdateIconBackgroundSound();
    }

    public void OnSound()
    {
        GameController.Instance.IsSoundOn = !GameController.Instance.IsSoundOn;
        UpdateIconSound();
    }

    public void OnMenu()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
    }
}
