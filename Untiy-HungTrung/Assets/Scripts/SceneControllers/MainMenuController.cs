﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    public GameObject mainCharacter;
    public Sprite[] characters;

	public void OnStart()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
        GameController.Instance.StartGame();
    }

    public void OnFacebookLogin()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
    }

    public void OnSetting()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
    }

    public void OnRemoveAds()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
    }

    public void OnChangeColor()
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
    }

    public void OnChooseCharacter(int characterID)
    {
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();

        GameController.Instance.ActiveCharacter = characterID;
        mainCharacter.GetComponent<Image>().sprite = characters[characterID];
    }
}
