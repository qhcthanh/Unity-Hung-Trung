﻿using UnityEngine;
using System.Collections;

public class SelectLevelController : MonoBehaviour {

	void Start () {
	
	}
	
	void Update () {
	
	}

    public void SelectLevel( int num )
    {
        GameObject.Find("MainGameController").GetComponent<MainGameController>().OnSelectLevel(num);
        if (GameController.Instance.soundController)
            GameController.Instance.soundController.playKaChingSound();
        GameController.Instance.StartGame();
    }
}
