﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoldText : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        int gold = GoldController.GetGold();
        GetComponent<Text>().text = string.Format("{0:#,###,###}", gold);
	}
}
